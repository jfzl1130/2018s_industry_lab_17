package ictgradschool.industry.testingandrefactoring.ex04;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Random;

public class Snake extends JFrame {

    private static final Random r = new Random();
    private Canvas c = new Canvas();
    private int fx = -1, fy = -1;
    private ArrayList<Integer> xs1 = new ArrayList<>();
    private ArrayList<Integer> xs2 = new ArrayList<>();
    private ArrayList<Integer> ys1 = new ArrayList<>();
    private ArrayList<Integer> ys2 = new ArrayList<>();
    private int d;
    private boolean done = false;


    void go() {

        while (!done) {
            int x2 = xs2.get(0);
            int y2 = ys2.get(0);
            if (d == 37) {
                x2--;
            }
            if (d == 39) {
                x2++;
            }
            if (d == 38) {
                y2--;
            }
            if (d == 40) {
                y2++;
            }
            if (x2 > 30 - 1) {
                x2 = 0;
            }
            if (x2 < 0) {
                x2 = 30 - 1;
            }
            if (y2 > 20 - 1) {
                y2 = 0;
            }
            if (y2 < 0) {
                y2 = 20 - 1;
            }
            boolean check11 = false;
            boolean check21 = false;
            check11 = isCheck11(x2, y2, check11);
            check21 = isCheck2(x2, y2, check21);
            done = check11 || check21;
            xs2.add(0, x2);
            ys2.add(0, y2);
            if (((xs2.get(0) == fx) && (ys2.get(0) == fy))) {
                fx = -1;
                fy = -1;
                setTitle("Program" + " : " + xs2.size());
            } else {
                xs2.remove(xs2.size() - 1);
                ys2.remove(ys2.size() - 1);
            }
            if (fx == -1) {
                int x, y;
                boolean check1 = false;
                boolean check2 = false;
                do {
                    x = r.nextInt(30);
                    y = r.nextInt(20);
                    check1 = isCheck11(x, y, check1);
                    check2 = isCheck2(x, y, check2);
                } while (check2 || check1);
                fx = x;
                fy = y;
                int x1, y1;
                boolean check3 = false;
                boolean check4 = false;
                do {
                    x1 = r.nextInt(30);
                    y1 = r.nextInt(20);
                    check3 = isCheck11(x1, y1, check3);
                    check4 = isCheck2(x1, y1, check4);
                } while (check3 || check4 || fx == x1 && fy == y1);

                xs1.add(x1);
                ys1.add(y1);
            }
            c.repaint();
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }

    private boolean isCheck2(int x, int y, boolean check2) {
        for (int i = 0; i < xs2.size(); i++) {
            if ((xs2.get(i) == x) && (ys2.get(i) == y)) {
                if (!((xs2.get(xs2.size() - 1) == x) && (ys2.get(ys2.size() - 1) == y))) {
                    check2 = true;
                }
            }
        }
        return check2;
    }

    private boolean isCheck11(int x2, int y2, boolean check11) {
        for (int i1 = 0; i1 < xs1.size(); i1++) {
            if (xs1.get(i1) == x2 && ys1.get(i1) == y2) {
                check11 = true;
            }
        }
        return check11;
    }

}

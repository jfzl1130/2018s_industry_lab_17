package ictgradschool.industry.testingandrefactoring.ex04;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Canvas extends JPanel {

    private int fx = -1, fy = -1;
    private ArrayList<Integer> xs1 = new ArrayList<>();
    private ArrayList<Integer> xs2 = new ArrayList<>();
    private ArrayList<Integer> ys1 = new ArrayList<>();
    private ArrayList<Integer> ys2 = new ArrayList<>();
    private boolean done = false;

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int i = 0; i < xs2.size(); i++) {
            g.setColor(Color.gray);
            g.fill3DRect(xs2.get(i) * 25 + 1, ys2.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
        }
        g.setColor(Color.green);
        g.fill3DRect(fx * 25 + 1, fy * 25 + 1, 25 - 2, 25 - 2, true);
        for (int i = 0; i < xs1.size(); i++) {
            g.setColor(Color.red);
            g.fill3DRect(xs1.get(i) * 25 + 1, ys1.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
        }
        if (done) {
            g.setColor(Color.red);
            g.setFont(new Font("Arial", Font.BOLD, 60));
            FontMetrics fm = g.getFontMetrics();
            g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
        }
    }

}

package ictgradschool.industry.testingandrefactoring.ex02;

public class Tdd {


    public double StringToDouble(String s) {

        double d;

        d = Double.parseDouble(s);

        return d;
    }

    public double AreaOfRectangle(double w, double  l) {


        double i = w*l;

        return i;
    }

    public double AreaOfCircle(double i) {

        double A = Math.PI*i*i;

        return A;
    }

    public int RoundingShape(double i) {


        return (int)(Math.round(i));
    }


    public int SmallerArea(int i, int j) {

        return Math.min(i,j);
    }
}

package ictgradschool.industry.testingandrefactoring.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TddTest {

    private Tdd shapeArea;

    @Before
    public void setUp() {
        shapeArea = new Tdd();
    }

    @Test
    public void testStringToDouble() {
        assertEquals(168.0,shapeArea.StringToDouble("168"),0.5);
    }
    @Test
    public void testAreaOfRectangle() {
        assertEquals(20.0, shapeArea.AreaOfRectangle(10.0, 2.0), 0.1);
    }
    @Test
    public void testAreaOfCircle() {
        assertEquals(3.14, shapeArea.AreaOfCircle(1),0.01);
    }
    @Test
    public void testRoundingShape() {
        assertEquals(168, shapeArea.RoundingShape(168),0.5);
    }
    @Test
    public void testSmallerArea() {
        assertEquals(168,shapeArea.SmallerArea(168,186),0.5 );
    }





}

package ictgradschool.industry.testingandrefactoring.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }


    @Test
    public void testTurnEast() {
        myRobot.turn();
        assertEquals(Robot.Direction.East, myRobot.getDirection());
    }

    @Test
    public void testTurnSouth() {
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.South, myRobot.getDirection());
    }

    @Test
    public void testTurnWest() {
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.West, myRobot.getDirection());
    }

    @Test
    public void testTurnNorth() {
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.North, myRobot.getDirection());
    }

    @Test
    public void testMoveEast() throws IllegalMoveException {
        try {
            myRobot.turn();
            myRobot.move();
            assertEquals(10, myRobot.currentState().row);
            assertEquals(2, myRobot.currentState().column);
        } catch (IllegalMoveException e) {
            fail();
        }

    }

    @Test
    public void testMoveSouth() throws IllegalMoveException {
        try {
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();
            assertEquals(10, myRobot.currentState().row);
            assertEquals(1, myRobot.currentState().column);
        } catch (IllegalMoveException e) {
            fail();
        }

    }

    @Test
    public void testMoveWest() throws IllegalMoveException {
        try {
            myRobot.move();
            myRobot.turn();
            myRobot.move();
            myRobot.turn();
            myRobot.move();
            myRobot.turn();
            myRobot.move();
            assertEquals(10, myRobot.currentState().row);
            assertEquals(1, myRobot.currentState().column);
        } catch (IllegalMoveException e) {
            fail();
        }

    }


    @Test
    public void testMoveNorth() throws IllegalMoveException {
        try {
            myRobot.move();

            assertEquals(9, myRobot.currentState().row);
            assertEquals(1, myRobot.currentState().column);
        } catch (IllegalMoveException e) {
            fail();
        }

    }

    @Test
    public void testIllegalMoveEast() {
        boolean atRight = false;
        try {
            myRobot.turn();

            // Move the robot to the right row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the right.
            atRight = myRobot.currentState().row == 10;
            assertTrue(atRight);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move East.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveSouth() {
        boolean atBottom = false;
        try {
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();

            // Check that robot has reached the bottom.
            atBottom = myRobot.currentState().row == 10;
            assertTrue(atBottom);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move South.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }


    @Test
    public void testIllegalMoveWest() {
        boolean atLeft = false;
        try {
            myRobot.turn();
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();

            // Check that robot has reached the left.
            atLeft = myRobot.currentState().row == 10;
            assertTrue(atLeft);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move left.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testBackTrack() throws IllegalMoveException {
        myRobot.move();
        myRobot.backTrack();
        assertEquals(10, myRobot.currentState().row);
        assertEquals(1,myRobot.currentState().column);


    }


}
